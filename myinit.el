;; activate line numbers
(global-linum-mode t)

;; rewires the movement and edit commands to work on CapitalizedWords
(global-subword-mode 1)

;; save the state of emacs when exiting
(desktop-save-mode 1)

;; backup in one place. flat, no tree structure
(setq backup-directory-alist '(("" . "~/.emacs.d/emacs-backup")))

;; reload files automatically if they are changed on disk
(global-auto-revert-mode t)

; (global-linum-mode t) ;; enable line numbers globally
;; if info mode disable line numbers
(add-hook 'Info-mode-hook (lambda () (linum-mode -1)))

;; clear buffer history - may need to run manualy
(setq recentf-list '())

;; run emacs server
(server-start)

;; workaround for mouse wheel to actual scroll
(global-set-key (kbd "C-M-(") (kbd "<mouse-4>"))
(global-set-key (kbd "C-M-)") (kbd "<mouse-5>"))

;; mouse wheel left or right will scroll as expected
(global-set-key (kbd "<mouse-6>") (lambda () (interactive) (scroll-right 10)))
(global-set-key (kbd "<mouse-7>") (lambda () (interactive) (scroll-left 10)))

;; horizontal scroll-bar
(horizontal-scroll-bar-mode t)

;; truncate line
(setq truncate-lines t)

;; use Trash for common deletion commands
(setq delete-by-moving-to-trash t)

;; Recentf mode - the File menu includes a submenu containing
;; a list of recently opened files
(recentf-mode)

;; C-n inserts newlines if point is at the end of the buffer
;; (setq next-line-add-newlines t)

(setq inhibit-startup-message t) ;; hide startup message

(tool-bar-mode 0) ;; hide toolbar

(fset 'yes-or-no-p 'y-or-n-p) ;; respond with y-or-n instead of yes-or-no

;; set the font
(add-to-list 'default-frame-alist
'(font . "Hermit-10.5:weight=medium"))

(require 'color-theme)
    (color-theme-initialize)

;;(load-theme 'material t)

(use-package try
    :ensure t)

(use-package which-key
    :ensure t
    :config
    (which-key-mode))

;; Org-mode stuff
(use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;;(setq ido-enable-flex-matching t)
;;(setq ido-everywhere t)
;;(ido-mode 1)

(defalias 'list-buffers 'ibuffer) ; make ibuffer default
(defalias 'list-buffers 'ibuffer-other-window) ; make ibuffer default

(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0))))) 
    ))

(winner-mode 1)

;; it looks like counsel is a requirement for swiper
(use-package counsel
:ensure t
)

(use-package swiper
:ensure try
:config
(progn
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-load-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
))

(use-package avy
:ensure t
:bind ("M-[" . avy-goto-char)) ; !!HERE IT BREAKS A DEFAULT EMACS KEY BINDING

;; initialize the package
(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    )) 

;; Do not run in python because of jedi
(add-hook 'python-mode-hook (lambda () (auto-complete-mode -1)))

;; Return key does not trigger autocomplete
(define-key ac-completing-map [return] nil)
(define-key ac-completing-map "\r" nil)

;; set python interpreter python version 3
(setq python-shell-interpreter "python3")

(use-package flycheck
:ensure t
:init
(global-flycheck-mode t))

(setq jedi:setup-keys t)

(use-package jedi
:ensure t
:init
(add-hook 'python-mode-hook 'jedi:setup)
(add-hook 'python-mode-hook 'jedi:ac-setup))
(setq jedi:complete-on-dot t) ; optional

;; (elpy-enable)
;; (defalias 'workon 'pyvenv-workon)

;; virtualenvwrapper
(require 'virtualenvwrapper)
(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support
;; note that setting `venv-location` is not necessary if you
;; use the default location (`~/.virtualenvs`), or if the
;; the environment variable `WORKON_HOME` points to the right place
(setq venv-location "/home/bogdan/Programming/virtualenvs")
;; Display on mode line
(setq-default mode-line-format (cons '(:exec venv-current-name) mode-line-format))
;; Eshell prompt

;; (defun my/python-mode-hook ()
;; (add-to-list 'company-backends 'company-jedi))

;; (add-hook 'python-mode-hook 'company-mode)
;; (add-hook 'python-mode-hook 'my/python-mode-hook)

;; (with-eval-after-load 'company
;; (define-key company-active-map (kbd "<return>") nil)
;; (define-key company-active-map (kbd "RET") nil)
;; (define-key company-active-map (kbd "C-SPC") #'company-complete-selection))

;; (pdf-tools-install)

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(setq neo-smart-open t)
(neotree-show)

(require 'all-the-icons)

(require 'jka-compr)
(setq tags-table-list
           '("/usr/share/emacs/26.1/lisp" "~/Programming/virtualenvs/pyside-test"))

;; show matching pairs
(show-paren-mode t)

;; ;; enable electric mode - minor mode for inserting matching delimiters
;; (electric-pair-mode t)

;; ;; makes the default pairing logic balance
;; ;; out the number of opening and closing delimiters
;; (setq electric-pair-preserve-balance t)

;; ;; makes backspacing between two adjacent delimiters
;; ;; also automatically delete the closing delimiter 
;; (setq electric-pair-delete-adjacent-pairs t)

;; ;; makes inserting a newline between two adjacent pairs 
;; ;; also automatically open an extra newline after point
;; (setq electric-pair-open-newline-between-pairs t)

;; ;; causes the minor mode to skip whitespace forward before
;; ;; deciding whether to skip over the closing delimiter
;; (setq electric-pair-skip-whitespace t)

(require 'smartparens-config)

    ;; these functions are too rigid - try in the future
    ;; (add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
    ;; (add-hook 'markdown-mode-hook 'turn-on-smartparens-strict-mode)

  (add-hook 'html-mode-hook #'smartparens-mode)
  (add-hook 'python-mode-hook #'smartparens-mode)

;; ensure installed
(require 'multi-term)

;; setup shell program for use with MultiTerm
(setq multi-term-program "/bin/bash")

;; set weeks to begin on Monday
(setq calendar-week-start-day 1)

;; Some commands need a terminal to display output properly (visual commands)
;; This are some example commands copied from elsewere, may need to modify
;; source: https://emacs.stackexchange.com/questions/34137/utf-8-characters-with-eshell-eshell-visual-subcommands-and-git
(eval-after-load "em-term"
  '(progn
     (add-to-list 'eshell-visual-commands "htop")
     (add-to-list 'eshell-visual-subcommands '("git" "diff" "help" "log" "show"))
     (add-to-list 'eshell-visual-options '("git" "--help" "--paginate"))
     (defalias 'ff 'find-file)
     (defalias 'ffo 'find-file-other-window)))

;; Open a new eshell buffer. With 'N it is given a positive integer,
;; with 'Z open the next sequential free shell number.
(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'Z))

(setq tramp-default-method "ssh")

(smartscan-mode 1)

(require 'org-journal)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-enable-current-element-highlight t)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)

(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

(move-text-default-bindings)

;; When you have an active region that spans multiple lines, 
;; the following will add a cursor to each line:
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)

;; When you want to add multiple cursors not based on continuous
;; lines, but based on keywords in the buffer, use:

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
